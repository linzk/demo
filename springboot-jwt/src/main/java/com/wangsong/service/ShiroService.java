package com.wangsong.service;

import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.wangsong.model.ShiroUserBean;
import com.wangsong.util.ShiroDataSource;

@Component
public class ShiroService {

    public ShiroUserBean getUser(String username) {
        // 没有此用户直接返回null
        if (! ShiroDataSource.getData().containsKey(username))
            return null;

        ShiroUserBean user = new ShiroUserBean();
        Map<String, String> detail = ShiroDataSource.getData().get(username);

        user.setUsername(username);
        user.setPassword(detail.get("password"));
        user.setRole(detail.get("role"));
        user.setPermission(detail.get("permission"));
        return user;
    }
}