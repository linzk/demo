import com.wangsong.Application;
import com.wangsong.RedisService;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;



@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class Test {

    @Autowired
    private RedisService redisService;

    @org.junit.Test
    public void test() throws InterruptedException {
        System.out.println(redisService.test3add("1"));
        Thread.sleep(1000*5);
        System.out.println(redisService.test3list("1"));
        Thread.sleep(1000*5);
        System.out.println(redisService.test3del("1"));
        Thread.sleep(1000*5);
        System.out.println(redisService.test3list("1"));
    }

}
