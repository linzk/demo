package com.wangsong;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class RedisService {

    @Cacheable(value = "test3", key = "#id")
    public Object test3list(String id) {
        System.out.println("查询数据库");
        return id;
    }

    @CacheEvict(value = "test3", key = "#id")
    public Object test3del(String id) {
        System.out.println("删除数据库");
        return id;
    }


    @CachePut(value = "test3", key = "#id")
    public Object test3add(String id) {
        System.out.println("增加数据库");
        return id;
    }
}
